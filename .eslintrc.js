// http://eslint.org/docs/user-guide/configuring

module.exports = {
    env: {
        browser: true,
        es6: true,
    },
    extends: [
        'eslint:recommended',
        'airbnb-base',
        'plugin:vue/recommended',
    ],
    globals: {
        Atomics: 'readonly',
        SharedArrayBuffer: 'readonly',
    },
    parserOptions: {
        parser: 'babel-eslint',
        ecmaVersion: 2018,
        sourceType: 'module',
    },
    plugins: [
        'vue',
    ],
    rules: {
        'arrow-parens': ['error', 'always'],
        'array-bracket-spacing': ['error', 'never'],
        'comma-dangle': ['error', 'always-multiline'],
        'import/extensions': ['error', 'never', {
            'js': 'never',
            'vue': 'never',
        }],
        'indent': ['error', 2],
        'linebreak-style': ['error', 'unix'],
        'max-len': 'off',
        'no-cond-assign': ['error', 'always'],
        'no-empty': ['error', { 'allowEmptyCatch': true }],
        'no-param-reassign': ['error', { 'props': false }],
        'no-plusplus': 'off',
        'no-trailing-spaces': ['error', { 'skipBlankLines': true }],
        'object-curly-spacing': ['error', 'never'],
        'prefer-destructuring': 'off',
        'radix': ['error', 'as-needed'],
        'space-in-parens': ['error', 'never'],
        'vue/html-self-closing': ['error', {
            'html': {
                'normal': 'never',
            },
        }],
        'vue/html-closing-bracket-spacing': ['error', {
            'selfClosingTag': 'never',
        }],
        'vue/html-indent': ['error', 4],
        'vue/script-indent': ['error', 2, {
            'baseIndent': 1,
            'switchCase': 1,
            'ignores': [],
        }],
        'vue/no-v-html': 'off',
    },
    'overrides': [
        {
            'files': ['*.vue'],
            'rules': {
                'indent': 'off',
            },
        },
    ],
    settings: {
        'import/resolver': {
            node: {
                extensions: ['.js', '.jsx', '.vue', '.scss'],
            }
        },
    },
};
