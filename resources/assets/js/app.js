import Vue from 'vue';
import ExampleComponent from './components/ExampleComponent';

window.Vue = Vue;

// eslint-disable-next-line no-unused-vars
const app = new Vue({
    el: '#app',
    components: {
        ExampleComponent,
    },
    render: (h) => h(ExampleComponent),
});
