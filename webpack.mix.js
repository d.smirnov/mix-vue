const mix = require('laravel-mix');
const imagemin = require('imagemin-keep-folder');
const imageminMozjpeg = require('imagemin-mozjpeg');
const imageminPngquant = require('imagemin-pngquant');
const IconfontPlugin = require('iconfont-plugin-webpack');
const IconfontPluginMixinPath = 'resources/assets/sass/helpers/_iconfont.scss';

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
mix.setPublicPath('./public')
    .js('resources/assets/js/app.js', 'public/js')
    .sass('resources/assets/sass/app.scss', 'public/css/app.css')
    .copyDirectory('resources/img', 'public/img')
    .browserSync({
        proxy: 'localhost:8080',
        ghostMode: false,
        watch: true,
        // files: [
        //     '!resources/assets/sass/helpers/_iconfont.scss',
        // ],
        // ignore: IconfontPluginMixinPath,
        watchOptions: {
            ignoreInitial: true,
            ignored: IconfontPluginMixinPath,
        },
    })
    .webpackConfig({
        plugins: [
            // new IconfontPlugin({
            //     src: 'resources/icons', // required - directory where your .svg files are located
            //     family: 'iconfont', // optional - the `font-family` name. if multiple iconfonts are generated, the dir names will be used.
            //     dest: {
            //         font: 'public/fonts/[family].[type]', // required - paths of generated font files
            //         css: 'resources/assets/sass/helpers/_iconfont.scss', // required - paths of generated css files //css: 'resources/assets/sass/helpers/_iconfont_[family].scss'
            //     },
            //     watch: {
            //         pattern: 'icons/**/*.svg', // required - watch these files to reload
            //         cwd: 'resources', // optional - current working dir for watching
            //     },
            //     // cssTemplate: function() {}// optional - the function to generate css contents
            // }),
        ],
    })
    .then(() => {
        if (mix.inProduction()) {
            const options = {
                plugins: [imageminMozjpeg({
                    quality: 100,
                    progressive: true,
                }), imageminPngquant({quality: [0.7, 1]})],
            };
            Promise.all([imagemin(['public/img/**/*.{jpg,jpeg,JPG,JPEG,png}'], options)])
                .catch((err) => console.error(err))
                .then((tasks) => {
                    const taskCount = tasks.length;
                    const fileCount = tasks.map((task) => task.length)
                        .reduce((a, b) => a + b);

                    console.log('Images compressed:');
                    console.log(`${taskCount} task(s) has been completed.`);
                    console.log(`${fileCount} file(s) has been compressed.`);
                });
        }
    });
